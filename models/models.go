package models

type Depts struct {
	IdDept int      `json:"id_dept`
	Name   string   `json:"name`
	Member []Member `json:"member"`
}
type Divs struct {
	IdDiv int    `json:"id_div`
	Name  string `json:"name`
}
type Roles struct {
	IdRole int    `json:"id_role`
	Name   string `json:"name`
}

type ProfiletEdit struct {
	IdRole int    `json:"id_role"`
	Name   string `json:"name"`
}
type Member struct {
	IdMember int     `json:"id_member"`
	Name     string  `json:"name"`
	Nik      string  `json:"nik"`
	Email    string  `json:"email"`
	Divs     []Divs  `json:"divs"`
	Roles    []Roles `json:"roles"`
	Active   string  `json:"active"`
	ImgURL   string  `json:"img_url"`
}

type ProfileDetail struct {
	IdToken string  `json:"id_token"`
	Name    string  `json:"name"`
	Email   string  `json:"email"`
	Nik     string  `json:"nik"`
	Roles   []Roles `json:"roles"`
	ImgURL  string  `json:"img_url"`
}
