package models

// Request

type RequestLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RequestReset struct {
	Email string `json:"email"`
}

type RequestChangePassword struct {
	IdToken     string `json:"id_token"`
	CurrentPass string `json:"current_pass"`
	Password    string `json:"password"`
}

type RequestDepartmentList struct {
	IdToken string `json:"id_token"`
	Offset  int    `json:"offset"`
	Limit   int    `json:"limit"`
}

type RequestDepartmentAdd struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
}

type RequestDepartmentEdit struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
	IdDept  int    `json:"id_dept`
}
type RequestDepartmentDelete struct {
	IdToken string `json:"id_token"`
	IdDept  int    `json:"id_dept`
}

type RequestDivisonList struct {
	IdToken string `json:"id_token"`
	IdDept  int    `json:"id_dept`
	Offset  int    `json:"offset"`
	Limit   int    `json:"limit"`
}

type RequestDivisonAdd struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
}

type RequestDivisonEdit struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
	IdDiv   int    `json:"id_div`
}
type RequestDivisonDelete struct {
	IdToken string `json:"id_token"`
	IdDiv   int    `json:"id_div`
}

type RequestRoleList struct {
	IdToken string `json:"id_token"`
	IdDiv   int    `json:"id_div`
	Offset  int    `json:"offset"`
	Limit   int    `json:"limit"`
}

type RequestRoleAdd struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
}

type RequestRoleEdit struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
	IdRole  int    `json:"id_role`
}
type RequestRoleDelete struct {
	IdToken string `json:"id_token"`
	IdRole  int    `json:"id_role`
}
type RequestProfileDetail struct {
	IdToken string `json:"id_token"`
}
type RequestProfileEdit struct {
	IdToken string `json:"id_token"`
	name    string `json:"name`
	Email   string `json:"email`
}
type RequestMemberCreate struct {
	IdToken  string `json:"id_token"`
	name     string `json:"name`
	Email    string `json:"email`
	Nik      int    `json:"nik`
	Password string `json:"password"`
	IdDept   int    `json:"id_dept`
	IdDiv    int    `json:"id_div`
	IdRole   int    `json:"id_role`
}
type RequestMemberListall struct {
	IdToken string `json:"id_token"`
	Offset  int    `json:"offset"`
	Limit   int    `json:"limit"`
}
type RequestMemberListbydept struct {
	IdToken string `json:"id_token"`
	IdDept  int    `json:"id_dept`
	Offset  int    `json:"offset"`
	Limit   int    `json:"limit"`
}
type RequestMemberEdit struct {
	IdToken  string `json:"id_token"`
	IdMember int    `json:"id_member`
	name     string `json:"name`
	Email    string `json:"email`
	Nik      int    `json:"nik`
	Password string `json:"password"`
	IdDept   int    `json:"id_dept`
	IdDiv    int    `json:"id_div`
	IdRole   int    `json:"id_role`
}
type RequestMemberDeactivate struct {
	IdToken  string `json:"id_token"`
	IdMember int    `json:"id_member`
}
type RequestMemberActivate struct {
	IdToken  string `json:"id_token"`
	IdMember int    `json:"id_member`
}

// Response

type Response struct {
	Error  string `json:"error"`
	Status string `json:"status"`
}

type ResponseLogin struct {
	Response
	IdToken string `json:"id_token"`
}
type ResponseReset struct {
	Response
}
type ResponseChangePassword struct {
	Response
}

type ResponseDepartmentList struct {
	Depts  []Depts `json:"depts"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
	Total  int     `json:"total"`
	Response
}

type ResponseDepartmentAdd struct {
	Response
}

type ResponseDepartmentEdit struct {
	Response
}
type ResponseDepartmentDelete struct {
	Response
}

type ResponseDivisionList struct {
	Divs   []Divs `json:"divs"`
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Total  int    `json:"total"`
	Response
}

type ResponseDivisionAdd struct {
	Response
}

type ResponseDivisionEdit struct {
	Response
}
type ResponseDivisionDelete struct {
	Response
}

type ResponseRoleList struct {
	Roles  []Roles `json:"roles"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
	Total  int     `json:"total"`
	Response
}

type ResponseRoleAdd struct {
	Response
}

type ResponseRoleEdit struct {
	Response
}
type ResponseRoleDelete struct {
	Response
}

type ResponseProfileDetail struct {
	Response
	ProfileDetail
}
type ResponseProfileEdit struct {
	Response
}

type ResponseMemberCreate struct {
	Response
}
type ResponseMemberListall struct {
	Depts  []Depts  `json:"depts"`
	Member []Member `json:"member"`
	Offset int      `json:"offset"`
	Limit  int      `json:"limit"`
	Total  int      `json:"total"`
	Response
}
type ResponseMemberListbydept struct {
	Member []Member `json:"member"`
	Offset int      `json:"offset"`
	Limit  int      `json:"limit"`
	Total  int      `json:"total"`
	Response
}
type ResponseMemberEdit struct {
	Response
}
type ResponseMemberDeactivate struct {
	Response
}
type ResponseMemberActivate struct {
	Response
}
