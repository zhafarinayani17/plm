package service

import (
	"PLM/models"
	"PLM/properties"
	"encoding/json"
	"fmt"
	"os"

	"PLM/gitlab.com/notula/go-tools/dbms"
	go_tools "PLM/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type PlmProfile struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *PlmProfile) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("profile_edit"), this.properties.Topic.GetQueueName("profile_edit"), this.profileEdit)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("profile_detail"), this.properties.Topic.GetQueueName("profile_detail"), this.profileDetail)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))
}

func (this *PlmProfile) profileEdit(msg *nats.Msg) {
	var response = models.ResponseProfileEdit{Response: models.Response{Status: "failed"}}
	request := &models.RequestProfileEdit{}
	this.logger.Info("profile_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("profile_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmProfile) profileDetail(msg *nats.Msg) {
	var response = models.ResponseProfileDetail{Response: models.Response{Status: "failed"}}
	request := &models.RequestProfileDetail{}
	this.logger.Info("profile_detail", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var profile = models.ProfileDetail{IdToken: "", Name: "Alyani", Email: "user@gmail.com", Nik: "4817", Roles: []models.Roles{models.Roles{IdRole: 1, Name: "Role"}}, ImgURL: "https://homepages.cae.wisc.edu/~ece533/images/cat.png"}
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(profile.Name) > 0 {
				response.Status = "succes"
				response.ProfileDetail = profile
			} else {
				response.Error = "package not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}
	this.logger.Info("profile_detail", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}
