package service

import (
	"PLM/models"
	"PLM/properties"
	"encoding/json"
	"fmt"
	"os"

	"PLM/gitlab.com/notula/go-tools/dbms"
	go_tools "PLM/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type PlmDivisionService struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *PlmDivisionService) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("division_list"), this.properties.Topic.GetQueueName("division_list"), this.divisionList)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("division_add"), this.properties.Topic.GetQueueName("division_add"), this.divisionAdd)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))

	subscription3, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("division_edit"), this.properties.Topic.GetQueueName("division_edit"), this.divisionEdit)
	this.logger.Info("subscribtion", zap.String("subject", subscription3.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription4, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("division_delete"), this.properties.Topic.GetQueueName("division_delete"), this.divisionDelete)
	this.logger.Info("subscribtion", zap.String("subject", subscription4.Subject), zap.Bool("status", subscription4.IsValid()))
}

func (this *PlmDivisionService) divisionList(msg *nats.Msg) {
	var response = models.ResponseDivisionList{Response: models.Response{Status: "failed"}}
	request := &models.RequestDivisonList{}
	this.logger.Info("division_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var packages = models.Divs{IdDiv: 1, Name: "Divisi Produk"}
		var arr_packages []models.Divs
		arr_packages = append(arr_packages, packages)
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(arr_packages) > 0 {
				response.Status = "success"
			} else {
				response.Error = "Divisi not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("division_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDivisionService) divisionAdd(msg *nats.Msg) {
	var response = models.ResponseDivisionAdd{Response: models.Response{Status: "failed"}}
	request := &models.RequestDivisonAdd{}
	this.logger.Info("division_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("division_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDivisionService) divisionEdit(msg *nats.Msg) {
	var response = models.ResponseDivisionEdit{Response: models.Response{Status: "failed"}}
	request := &models.RequestDivisonEdit{}
	this.logger.Info("division_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("division_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDivisionService) divisionDelete(msg *nats.Msg) {
	var response = models.ResponseDivisionDelete{Response: models.Response{Status: "failed"}}
	request := &models.RequestDivisonDelete{}
	this.logger.Info("division_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("division_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
