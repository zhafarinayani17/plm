package service

import (
	"PLM/models"
	"PLM/properties"
	"encoding/json"
	"fmt"
	"os"

	"PLM/gitlab.com/notula/go-tools/dbms"
	go_tools "PLM/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type PlmMemberService struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *PlmMemberService) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_create"), this.properties.Topic.GetQueueName("member_create"), this.memberCreate)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_listall"), this.properties.Topic.GetQueueName("member_listall"), this.memberListall)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))

	subscription3, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_listbydept"), this.properties.Topic.GetQueueName("member_listbydept"), this.memberListbydept)
	this.logger.Info("subscribtion", zap.String("subject", subscription3.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription4, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_edit"), this.properties.Topic.GetQueueName("member_edit"), this.memberEdit)
	this.logger.Info("subscribtion", zap.String("subject", subscription4.Subject), zap.Bool("status", subscription4.IsValid()))

	subscription5, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_deactivate"), this.properties.Topic.GetQueueName("member_deactivate"), this.memberDeactivate)
	this.logger.Info("subscribtion", zap.String("subject", subscription5.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription6, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("member_activate"), this.properties.Topic.GetQueueName("member_activate"), this.memberActivate)
	this.logger.Info("subscribtion", zap.String("subject", subscription6.Subject), zap.Bool("status", subscription4.IsValid()))
}

func (this *PlmMemberService) memberCreate(msg *nats.Msg) {
	var response = models.ResponseMemberCreate{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberCreate{}
	this.logger.Info("member_create", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_create", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmMemberService) memberListall(msg *nats.Msg) {
	var response = models.ResponseMemberListall{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberListall{}
	this.logger.Info("member_listall", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var packages = models.Member{IdMember: 1, Name: "Alyani Zhafrina", Nik: "4187070013", Email: "zahafrinayani17", Divs: []models.Divs{models.Divs{IdDiv: 1, Name: "Divisi"}}, Roles: []models.Roles{models.Roles{IdRole: 1, Name: "Role"}}, Active: "true", ImgURL: "https://homepages.cae.wisc.edu/~ece533/images/cat.png"}
		var arr_packages []models.Member
		arr_packages = append(arr_packages, packages)
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(arr_packages) > 0 {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_listall", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmMemberService) memberListbydept(msg *nats.Msg) {
	var response = models.ResponseMemberListbydept{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberListbydept{}
	this.logger.Info("member_listbydept", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var packages = models.Member{IdMember: 1, Name: "Alyani Zhafrina", Nik: "4187070013", Email: "zahafrinayani17", Divs: []models.Divs{models.Divs{IdDiv: 1, Name: "Divisi"}}, Roles: []models.Roles{models.Roles{IdRole: 1, Name: "Role"}}, Active: "false", ImgURL: "https://homepages.cae.wisc.edu/~ece533/images/cat.png"}
		var arr_packages []models.Member
		arr_packages = append(arr_packages, packages)
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(arr_packages) > 0 {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_listbydept", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmMemberService) memberEdit(msg *nats.Msg) {
	var response = models.ResponseMemberEdit{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberEdit{}
	this.logger.Info("member_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmMemberService) memberDeactivate(msg *nats.Msg) {
	var response = models.ResponseMemberDeactivate{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberDeactivate{}
	this.logger.Info("member_deactivate", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_deactivate", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmMemberService) memberActivate(msg *nats.Msg) {
	var response = models.ResponseMemberActivate{Response: models.Response{Status: "failed"}}
	request := &models.RequestMemberActivate{}
	this.logger.Info("member_activate", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Member not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("member_activate", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}
