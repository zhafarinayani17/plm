package service

import (
	"PLM/models"
	"PLM/properties"
	"encoding/json"
	"fmt"
	"os"

	"PLM/gitlab.com/notula/go-tools/dbms"
	go_tools "PLM/gitlab.com/notula/go-tools2"

	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
)

type PlmDepartmentService struct {
	nats       *nats.Conn
	dbms       *dbms.DatabaseRepository
	logger     *go_tools.Logger
	properties *properties.ServiceProperties
}

func (this *PlmDepartmentService) Init(properties *properties.ServiceProperties) {
	this.properties = properties
	var err error
	this.logger, err = go_tools.LoggerRotateGenerator(this.properties.Logging.OutputDir, this.properties.Logging.ErrorOutputDir, this.properties.Logging.Name, this.properties.Logging.Debug)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.nats, err = nats.Connect(this.properties.Nats.Address)
	if err != nil {
		this.logger.Error(err.Error())
		os.Exit(1)
	}

	// dbm := dbms.New(this.properties.Database, dbms.POSTGRESQL)
	// e := dbm.Connect()
	// if e != nil {
	// 	this.logger.Info("dbms_connect", zap.String("error", e.Error()))
	// }
	// dbm.Database.LogMode(true)

	subscription, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_list"), this.properties.Topic.GetQueueName("department_list"), this.departmentList)
	this.logger.Info("subscribtion", zap.String("subject", subscription.Subject), zap.Bool("status", subscription.IsValid()))

	subscription2, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_add"), this.properties.Topic.GetQueueName("department_add"), this.departmentAdd)
	this.logger.Info("subscribtion", zap.String("subject", subscription2.Subject), zap.Bool("status", subscription2.IsValid()))

	subscription3, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_edit"), this.properties.Topic.GetQueueName("department_edit"), this.departmentEdit)
	this.logger.Info("subscribtion", zap.String("subject", subscription3.Subject), zap.Bool("status", subscription3.IsValid()))

	subscription4, _ := this.nats.QueueSubscribe(this.properties.Topic.GetChannel("department_delete"), this.properties.Topic.GetQueueName("department_delete"), this.departmentDelete)
	this.logger.Info("subscribtion", zap.String("subject", subscription4.Subject), zap.Bool("status", subscription4.IsValid()))
}

func (this *PlmDepartmentService) departmentList(msg *nats.Msg) {
	var response = models.ResponseDepartmentList{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentList{}
	this.logger.Info("department_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var packages = models.Depts{IdDept: 1, Name: "Divisi Produk"}
		var arr_packages []models.Depts
		arr_packages = append(arr_packages, packages)
		if err != nil {
			response.Error = err.Error()
		} else {
			if len(arr_packages) > 0 {
				response.Status = "success"
			} else {
				response.Error = "service not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("department_list", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDepartmentService) departmentAdd(msg *nats.Msg) {
	var response = models.ResponseDepartmentAdd{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentAdd{}
	this.logger.Info("department_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("department_add", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, err := json.Marshal(response)
	if err != nil {
		this.logger.Error(err.Error())
	}
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDepartmentService) departmentEdit(msg *nats.Msg) {
	var response = models.ResponseDepartmentEdit{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentEdit{}
	this.logger.Info("department_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("department_edit", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}

func (this *PlmDepartmentService) departmentDelete(msg *nats.Msg) {
	var response = models.ResponseDepartmentDelete{Response: models.Response{Status: "failed"}}
	request := &models.RequestDepartmentDelete{}
	this.logger.Info("department_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)))
	if err := json.Unmarshal(msg.Data, &request); err == nil {
		var result = true
		if err != nil {
			response.Error = err.Error()
		} else {
			if result {
				response.Status = "success"
			} else {
				response.Error = "Divison not found"
			}
		}
	} else {
		response.Error = "unrecognize request"
	}

	this.logger.Info("department_delete", zap.String("subject", msg.Subject), zap.String("raw_request", string(msg.Data)), zap.Any("response", response))
	bytes, _ := json.Marshal(response)
	this.nats.Publish(msg.Reply, bytes)
}